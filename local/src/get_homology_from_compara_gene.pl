#!/usr/bin/env perl
# 
# Filename: 
# Description: 
# Author: Michele
# Maintainer: 
# Created: Fri May 20 12:58:53 2011 (+0200)
# Version: 
# Last-Updated: 
#           By: 
#     Update #: 0
# URL: 
# Keywords: 
# Compatibility: 
# 
# 

# Commentary: 
# 
# 
# 
# 

# Change Log:
# 
# 
# 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth
# Floor, Boston, MA 02110-1301, USA.
# 
# 

# Code:
use strict;
use Bio::EnsEMBL::Registry;
# Function to print Object properties
use Data::Dumper;
# C like struct for containing data
use Class::Struct;
use Bio::SeqIO;
use Getopt::Long;


$SIG{__WARN__} = sub {die @_};

my $usage = qq{
Usage: $0 [-h|help] [g|gene_name]

Search "gene_name" into Ensembl core by external name. Search is case-insensitive.
For each gene found, search if it's involved in some relation of homology (orthology or paralogy), in Ensembl compara.
All homolgy relations (pairwise relations) are extracted.
For each member of a pairwise relation corresponding gene is indentified.
For each member also all gene families the memeber correspond to, along with annotation confidence scores, are identified.
For each gene all transcripts are determined.
For each coding transcript translated peptide is printed in fasta format on STDOUT.

Each fasta sequence header contains following informations:
peptide_stable_id specie homology families&scores gene_stable_id gene_name transcript_stable_id translation_stable_id (identical to peptide_stable_id)

The set is made non-redundat on the base of gene stable ID.

Options:
   -h, --help        this help
   -s, --specie      [string] starting specie DB in which search gene_name. 
                     Default "Human".
   -g, --gene_name   [string] gene name to be queried. SQL wildcards (%) and (_) 
                     are supported. Default "DMRT%".
       
};



# test if no argument are given
if ((!@ARGV)) {
    print "$0: Argument required.\n";
    print $usage;
    exit 1
}

# initialize argument containers
my $gene_name = 'DMRT%';
my $specie = 'Human';
my $help = undef;


# s=string type
# i=integer type
GetOptions(
    "h|help" => \$help,
    "s|specie=s" => \$specie,
    "g|gene_name=s" => \$gene_name
) or die($usage);


# Print Help and exit
if ($help) {
    print $usage;
    exit(0);
}



# set up registry
my $registry = 'Bio::EnsEMBL::Registry';
$registry->load_registry_from_db(
	-host => 'ensembldb.ensembl.org',
	-user => 'anonymous'
);



# get obj adaptors
my $family_adaptor = $registry->get_adaptor('Multi','compara','Family');
my $member_adaptor = $registry->get_adaptor('Multi', 'compara', 'Member');
my $homology_adaptor = $registry->get_adaptor('Multi', 'compara', 'Homology');
my $gene_adaptor = $registry->get_adaptor($specie, "core", "Gene");



# prototype of C like struct for containing data
struct( Member => {
    member_stableID => '$',
    member_desc => '$',
    member_taxon => '$',
    member_specie => '$',
    member_family_desc_score => '$',
    member_homolog_to => '$',
    member_homology_desc => '$',
    member_gene => '$'
	});


# empty hash
my %hash_all_memb = {};

# search gene by external_name in DB defined by $specie
my $genes = $gene_adaptor->fetch_all_by_external_name($gene_name);

foreach my $gene (@{$genes})
{
    # search if given gene is involved in some relation of homology 
    # (orthology or paralogy)
    my $member = $member_adaptor->fetch_by_source_stable_id("ENSEMBLGENE",$gene->stable_id);
    if ($member)
    {
	# get all homology relations in which this gene is involved
	my $homologies = $homology_adaptor->fetch_all_by_Member($member);
	
	foreach my $homology (@{$homologies}) 
	{
	    # get all members of this homology relation 
            # (for now they are pairwise relations in which one member is $member)
	    my $homology_members = $homology->get_all_Members();
	    
	    foreach my $homology_memb (@{$homology_members})
	    {
		# test if the member is non-empty
		if ($homology_memb)
		{
		    my @family_desc_score;

		    # if any, get all gene families the current memeber belong to
		    my $families = $family_adaptor->fetch_all_by_Member($homology_memb);
		    foreach my $family (@{$families})
		    {
			# put in the same array 
			# decription and description score for the current family
			push(@family_desc_score, join("_", map { $family->$_ }  qw(description description_score)));
		    }
		    
		    # save all data on struct
		    my $homology_memb_dat = Member->new( 
			member_stableID => $homology_memb->stable_id(),
			member_desc => $homology_memb->description(),
			member_taxon => $homology_memb->taxon,
			member_specie => $homology_memb->taxon->binomial,
			member_family_desc_score => join(':', @family_desc_score),
			member_homolog_to => $member->stable_id(),
			member_homology_desc => $homology->description(),
			member_gene => $homology_memb->get_Gene()			);

		    #print "member stableID ".$homology_memb_dat->member_stableID."\n";
		    #print "member description ".$homology_memb_dat->member_desc."\n";
		    #print "member specie ".$homology_memb_dat->member_specie."\n";
		    #print "member source ".$homology_memb_dat->member_source;
		    #print "member family desc-score ".$homology_memb_dat->member_family_desc_score;
		    #print "member homology desc ".$homology_memb_dat->member_homology_desc."\n";
		    #print "member gene ".$homology_memb_dat->member_gene."\n"; 
		    #print;

		    # $hash{ 'key' } = 'value';    # hash
		    # different genes can share homologous
		    # use $homology_memb->stable_id() as key in a hash to eliminate 
		    # redundant homologous for the family
		    $hash_all_memb{ $homology_memb_dat->member_stableID } = $homology_memb_dat;

		}
	    }
	}
    }   
}




while ( my ($key, $homology_memb_dat) = each(%hash_all_memb) ) 
{
    if ($homology_memb_dat)
    {
	# get stable ID for gene of the current member
	my $gene_stableID = $homology_memb_dat->member_gene->stable_id;
	# gene external name
	my $gene_external_name = $homology_memb_dat->member_gene->external_name();
	# all transcripts
	my $transcripts = $homology_memb_dat->member_gene->get_all_Transcripts();

	foreach my $transcript (@{$transcripts})
	{
	    # current transcripts stable ID
	    my $transcript_stableID = $transcript->stable_id();

	    # check if transcript has translation (is protein coding)
	    if ($transcript->translation())
	    {
		# peptide stable ID
		my $translation_stableID = $transcript->translation()->stable_id();
	    
		# get aminoacid sequence
		my $peptide = $transcript->translate();
		#my $uniprot_xref = join("_", $transcript->translation()->get_all_DBEntries('Uniprot%'));

		# pass arguments to the subroutine as 
		# "named arguments" by using a hash reference.
		&write_seq({
		    homology_member_desc => $homology_memb_dat->member_desc,
		    homology_member_specie => $homology_memb_dat->member_specie,
		    homolog_to => $homology_memb_dat->member_homolog_to,
		    homology_desc => $homology_memb_dat->member_homology_desc,
		    family_desc_score => $homology_memb_dat->member_family_desc_score,
		    gene_stableID => $gene_stableID,
		    gene_external_name => $gene_external_name,
		    transcript_stableID => $transcript_stableID,
		    translation_stableID => $translation_stableID,
		    peptide => $peptide
			   });

		# print "gene stable id ".$gene_stableID;
		# print "gene external name ".$gene_external_name;
		# print "transcript stable id ".$transcript_stableID;
		# print "translation stable ID ".$translation_stableID;
		# print "peptide ".$peptide;
		# print "";
	    }

	} 

    }
}




sub write_seq
{
    my ($args) = @_;

    # sobstitute witespace and tab with _
    my $homology_member_specie = $args->{homology_member_specie}; #=~ s/(\s|\t)/_/);
    $homology_member_specie =~ s/(\s|\t)/_/g;
    my $homolog_to = $args->{homolog_to};
    $homolog_to =~ s/(\s|\t)/_/g;
    my $homology_desc = $args->{homology_desc};
    $homology_desc =~ s/(\s|\t)/_/g;
    my $family_desc_score = $args->{family_desc_score};
    $family_desc_score =~ s/(\s|\t)/_/g;
    my $gene_external_name = $args->{gene_external_name};
    $gene_external_name =~ s/(\s|\t)/_/g;
    

    # $args->{parameter}
    $args->{peptide}->id( join(" ", ($args->{peptide}->id,
				     "specie:".$homology_member_specie,
				     "homolog_to:".$homolog_to,
				     "homology_desc:".$homology_desc,
				     "families&scores:".$family_desc_score,
				     "gene:".$args->{gene_stableID},
				     "gene_name:".$gene_external_name,
				     "cdna:".$args->{transcript_stableID},
				     "translation:".$args->{translation_stableID}
			       ) ) );

    my $stream = Bio::SeqIO->new(
    	-format => 'Fasta',
#    	-file => ">>pluto.fasta",
    	-fh     => \*STDOUT, # write to STDOUT
    	);

    $stream->write_seq($args->{peptide});
}

