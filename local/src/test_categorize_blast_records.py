# test_categorize_blast_records.py --- 
# 
# Filename: test_categorize_blast_records.py
# Description: 
# Author: Michele
# Maintainer: 
# Created: Tue Jun 14 11:18:26 2011 (+0200)
# Version: 
# Last-Updated: 
#           By: 
#     Update #: 0
# URL: 
# Keywords: 
# Compatibility: 
# 
# 

# Commentary: 
# 
# 
# 
# 

# Change Log:
# 
# 
# 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth
# Floor, Boston, MA 02110-1301, USA.
# 
# 

# Code:

#
import sys,os,argparse
#
import categorize_blast_records



def main(arguments):
    
    sys.argv = arguments
    
    args = arg_parse()
 

    xml =  args.blast_results

    seq_lsts = args.file_lsts
    # # first sequence list file name with aboslute path
    # contig_lst_1 = os.path.abspath(arguments[2])
    # seq_lsts.append(contig_lst_1)
    # # second sequence list file name with aboslute path
    # contig_lst_2 = os.path.abspath(arguments[3])
    # seq_lsts.append(contig_lst_2)
    # # second sequence list file name with aboslute path
    # contig_lst_3 = os.path.abspath(arguments[4])
    # seq_lsts.append(contig_lst_3)


    # categorize_blast_records divide the Blast records in categories,
    # depending on the name of the subjects. The names of the subjects
    # are given in two lists. Another category consists of records whose
    # subject does not fall into any of the other lists
    command = r"categorize_blast_records.py --xml %s --lsts %s"\
              %(xml, " ".join(seq_lsts))

    print command

    # return list of blast record lists
    blast_record_chunks = categorize_blast_records.main(command.split())

    for key in blast_record_chunks.keys():
        blast_record_chunk = blast_record_chunks[key]
        
        print "\n", key

        for blast_record in blast_record_chunk:
        
            for alignment in blast_record.alignments:
                
                print alignment.hit_def


    return 0




def arg_parse():
    """
    Parses command line arguments
    """
    parser = argparse.ArgumentParser(description="Divides Bio.Blast.Record-s according to subject sequences listed in various files.")
    parser.add_argument("--xml", "-x", dest="blast_results", required=True, help="Blast results in xml format")
    parser.add_argument("--lsts", "-l", dest="file_lsts", nargs='+', required=True, help="Lists of contigs in text format")

    args = parser.parse_args()
    
    return args



if __name__ == "__main__":
    main(sys.argv)

# 
# test_categorize_blast_records.py ends here
