# filter_blast_results.py --- 
# 
# Filename: filter_blast_results.py
# Description: 
# Author: Michele
# Maintainer: 
# Created: Tue Jun  7 20:06:01 2011 (+0200)
# Version: 
# Last-Updated: 
#           By: 
#     Update #: 0
# URL: 
# Keywords: 
# Compatibility: 
# 
# 

# Commentary: 
# 
# 
# 
# 

# Change Log:
# 
# 
# 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth
# Floor, Boston, MA 02110-1301, USA.
# 
# 

# Code:
import sys,os,time,shutil,argparse
#
import blast2filter, categorize_blast_records

def main(arguments):

    sys.argv = arguments
    
    args = arg_parse()
    
    gene_lst = [     # Sex Determination in the Squalius alburnoides Complex: An Initial Characterization of Sex Cascade Elements in the Context of a Hybrid Polyploid Genome
     r'DMRT',\
     r'WT1',\
     r'DAX',\
     r'FIGLA',\
     # Discovery in sturgeon
     r'SOX',\
     r'RSPO',\
     r'WNT',\
     r'FOX',\
     r'TRA',\
     r'FEM',\
     # Amh and Dmrta2 Genes Map to Tilapia (Oreochromis spp.) Linkage Group 23 Within Quantitative Trait Locus Regions for Sex Determination
     # r'SF',\
     r'CYP',\
     r'AMH',\
     r'FHL',\
     r'LHX',\
     # The evolution of sex chromosomes and sex determination in vertebrates and the key role of DMRT1
     r'GATA',\
     r'ATRX',\
     r'FGF',\
     r'TDA',\
     r'DHTR',\
     r'PAX',\
     r'LIM',\
     r'EMX',\
     r'INSL',\
     r'DHH'\
        ]

    contig_lst = []
    for file_lst in args.file_lsts:
        # list file name with aboslute path
        contig_lst.append(os.path.abspath(file_lst))

    # thresholds
    E_VALUE_THRESH = float(args.e_value_thresh)
    QUERY_COV = float(args.query_cov)
    # final report file
    csv_file = open("presence-absence-table.csv", u"w")
    csv_file.write("QUERY,QUERY_LENGTH,FRACTION_QUERY_COVERED,SUBJECT,SBJ_LENGTH,FRACTION_SBJ_COVERED,EXPECT,ALIGN_LENGTH,FRAC_ALN_IDENTITIES")
    ###
    
    current_dir = os.getcwd()

    for gene in gene_lst:

        dir_name = gene + r"_redundant"

        csv_file.write(u"\nGenes Category: %s %s,\n" %(gene, "_".join(dir_name.split(r"_")[1:])))

        if os.path.isdir(dir_name) ==  True:
            os.chdir(dir_name)

        dir_lst = os.listdir(u".")

        for file_name in dir_lst:
            # there could be more than 1 "." in
            # a file name
            if len(file_name.split(r".")) >= 2:
                # get extension starting from end
                extension = file_name.split(r".")[-1]

                # remove precedent files generated producing
                # the report file (.pdf, .log, .aux)
                rm_report_files(file_name, extension)

                # I base opening xml files on the fasta
                # file that acts as a query, i.e. the gene
                if extension == r"fasta":
                    # get file name as the rest of list other than
                    # extension
                    fasta_name = ".".join(file_name.split(r".")[:-1])

                    csv_file.write(u"\n%s,\n" %(fasta_name))

                    # divide the Blast records in categories
                    blast_record_chunks, parent_xmls = categorize_Brecords(fasta_name, contig_lst)
                    # iterate over blast_record_chunks vocabolary
                    for chunk_key in blast_record_chunks.keys():

                        blast_record_chunk = []
                        chunk_name = []
                        matching_queries = []

                        # list of Bio.Blast.Record
                        blast_record_chunk = blast_record_chunks[chunk_key]

                        # composes the name of the chunk
                        chunk_name = chunk_name_composer(chunk_key, \
                                                         len(blast_record_chunk), \
                                                         dir_name, \
                                                         fasta_name, \
                                                         E_VALUE_THRESH)
                        
                        csv_file.write(u"%s,\n" % chunk_name)

                        # categorize blast records
                        matching_queries = filter_Brecords(blast_record_chunk, \
                                                           E_VALUE_THRESH, \
                                                           QUERY_COV, \
                                                           parent_xmls, \
                                                           chunk_name)
                        
                        # print query that satisfies the filter
                        # conditions
                        match_print(matching_queries, csv_file)
                    
                # remove precedent blast db files
                # rm_blastDB_files(file_name, extension)
                                   
        os.chdir(current_dir)

    csv_file.close()
    
    return 0




#
def chunk_name_composer(chunk_key, chunk_length, dir_name, fasta_name, E_VALUE_THRESH):
    """
    Composes the name of the chunk from various elements
    """
    # obtained the ending from the type of folder inspected: _redundant or _non_redundant
    ending = r"_".join(dir_name.split(r"_")[1:])

    chunk_name = r"%s_%i_%s_%.2e_%s" %(chunk_key, chunk_length, fasta_name, E_VALUE_THRESH, ending)
    
    return chunk_name



def rm_blastDB_files(file_name, extension):
    """
    Remove precedent blast db files
    """  
    if extension == r"phd" or extension == r"phi" or extension == r"phr" or \
           extension == r"pin" or extension == r"pog" or extension == r"psd" or \
           extension == r"psi" or extension == r"psq" or extension == r"pal":
        os.remove(file_name)
        
    return 0


def rm_report_files(file_name, extension):
    """
    Remove precedent latex report files
    """  
    if extension == r"pdf" or extension == r"log" or extension == r"aux":
        os.remove(file_name)
        
    return 0




def categorize_Brecords(fasta_name, seq_lsts):
    """
    Categorize_blast_records divide the Blast records in categories,
    depending on the name of the subjects. The names of the subjects
    are given in two lists. Another category consists of records whose
    subject does not fall into any of the other lists
    """    
    # I reconstruct the names of the corresponding
    # xml files by the name of the gene
    parent_xmls = [r"CDNA3-4_assembly_%s_10_redundant.xml" %(fasta_name)]
    

    # then I loop over the reconstructed xml file
    for xml in parent_xmls:

        # To call one python script from Within Another python script is enough:

        # import script name
        # script_name.main (command_line_arguments.split ())

        # as I pass command line argument list equal to the output of sys.argv

        # compose command line arguments to be passed to
        # categorize_blast_records.py
        command = r"categorize_blast_records.py --xml %s --lsts %s"\
                  %(xml, " ".join(seq_lsts))

        print command

        # return a dictionary of blast record lists
        blast_record_chunks = categorize_blast_records.main(command.split())

    return blast_record_chunks, parent_xmls






def filter_Brecords(blast_record_chunk, E_VALUE_THRESH, QUERY_COV, parent_xmls, chunk_name):
    """
    Filters Bio.Blast.Record chunk list and produces a report file in
    pdf format for each one
    """
    # To call one python script from Within Another python script is enough:

    # import script name
    # script_name.main (command_line_arguments.split ())

    # as I pass command line argument list equal to the output of sys.argv

    # compose command line arguments to be passed to
    # blast2filter.py
    command = r"blast2filter.py --xmls %s --eval %.2e --query_cov %.2f --rep_file %s"\
              %(parent_xmls[0], E_VALUE_THRESH, QUERY_COV, chunk_name)

    print command
    
    # pass command line arguments and Bio.Blast.Record list to
    # blast2filter.main()
    matching_queries = blast2filter.main(command.split(), blast_record_chunk)

    return matching_queries



def match_print(matching_queries, csv_file):
    """
    Print query that satisfies the filter conditions
    """
    # content control
    if matching_queries.keys():
        for query in matching_queries.keys():
            hit = matching_queries[query]
            # format line to print
            csv_file.write(u"%s,%i,%.2f,%s,%i,%.2f,%.2e,%i,%.2f\n" %(hit.query, \
                                                             hit.query_length, \
                                                             hit.frac_query_cov, \
                                                             hit.subject, \
                                                             hit.sbjct_length, \
                                                             hit.frac_sbj_cov, \
                                                             hit.expect, \
                                                             hit.align_length, \
                                                             hit.frac_aln_identities\
                                                             ))

    return 0





def arg_parse():
    """
    Parses command line arguments
    """
    parser = argparse.ArgumentParser(description="")
    #parser.add_argument("--xml", "-x", dest="blast_results", required=True, help="Blast results in xml format")
    parser.add_argument("--lsts", "-l", dest="file_lsts", nargs='+', required=True, help="Lists of contigs in text format")
    parser.add_argument("--eval", "-e", dest="e_value_thresh", default=10, required=False, type=float, help="E-value threshold")
    parser.add_argument("--query_cov", "-q", dest="query_cov", default=0, required=False, type=float, help="% query coverage threshold")

    args = parser.parse_args()
    
    return args






if __name__ == "__main__":
    main(sys.argv)

# 
# filter_blast_results.py ends here










