# blast2filter.py --- 
# 
# Filename: blast2filter.py
# Description: 
# Author: Michele
# Maintainer: 
# Created: Mon Jun 13 16:04:28 2011 (+0200)
# Version: 
# Last-Updated: 
#           By: 
#     Update #: 0
# URL: 
# Keywords: 
# Compatibility: 
# 
# 

# Commentary: 
# 
# 
# 
# 

# Change Log:
# 
# 
# 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth
# Floor, Boston, MA 02110-1301, USA.
# 
#



# Code:
from Bio.Blast import NCBIXML
# designed for drawing whole genomes diagrams.
# new in Biopython 1.50 and later
# depends on the third party Python library ReportLab
from Bio.Graphics import GenomeDiagram
# features of seq record objects from Biopython
from Bio.SeqFeature import SeqFeature, FeatureLocation
# control diagrams colors
from reportlab.lib import colors
# measurement units
from reportlab.lib.units import cm
# utility modules
import sys, random, base64, shutil, os, subprocess, math
# argument parser
import argparse



# container for blast hit attributes
class Hit:
    def __init__(self,
                 application,\
                 database,\
                 blast_expect,\
                 subject,\
                 query,\
                 query_length,\
                 align_length,\
                 expect,\
                 query_start,\
                 query_end,\
                 sbjct_length,\
                 sbjct_start,\
                 sbjct_end,\
                 hit_id,\
                 identities,\
                 gaps,\
                 frame,\
                 score,\
                 frac_query_cov = 0.0,\
                 frac_sbj_cov = 0.0,\
                 frac_aln_identities = 0.0):

        self.application = str(application)
        self.database = str(os.path.basename(database))
        self.blast_expect = float(blast_expect)
        self.expect = float(expect)
        self.query = str(query)
        self.query_length = int(query_length)
        self.query_start = int(query_start )
        self.query_end = int(query_end)
        self.frac_query_cov = float(frac_query_cov)
        self.subject = str(subject)
        self.sbjct_start = int(sbjct_start)
        self.sbjct_end = int(sbjct_end)
        self.sbjct_length = int(sbjct_length)
        self.frac_sbj_cov = float(frac_sbj_cov)
        self.align_length = int(align_length)
        self.identities = int(identities)
        self.gaps = int(gaps)
        self.hsp_score = float(score)
        self.frac_aln_identities = float(frac_aln_identities)
        self.hit_id = str(hit_id)
        self.frame = frame
    # Called by built-in function hash() and for
    # Operations on members of hashed collections including set.
    # Should return an integer.
    # It required that objects which compare
    # equal have the same hash value;
    # return hash value calculated from the
    # hit_id object
    def __hash__(self):
        return hash(self.hit_id)
    # MODIFICARE L'HASH
    
    # A class that define a __hash__() operation
    # should define __cmp__() or __eq__() method also.
    # Overload of the method names that permits to use
    # the operator symbols x==y calls
    
    # comparison based on the name of the subject
    def __eq__(self, other):
        if self.subject == other.subject:
            return True
        return False
    # Called by the str() built-in function and
    # by the print statement to compute the "informal"
    # string representation of an object.
    def __str__(self):
        return "application: %s\n" %(self.application) +\
        "database: %s\n" %(self.database) +\
        "blast expect: %.2e\n" %(self.blast_expect) +\
        "query: %s -> subject %s\n" %(self.query, self.subject) +\
        "query_length: %i\n" %(self.query_length) +\
        "align_length: %i\n" %(self.align_length) +\
        "expect: %.2e\n" %(self.expect) +\
        "query_start: %i\n" %(self.query_start) +\
        "query_end: %i\n" %(self.query_end) +\
        "sbjct_length %i\n" %(self.sbjct_length) +\
        "sbjct_start: %i\n" %(self.sbjct_start) +\
        "sbjct_end: %i\n" %(self.sbjct_end) +\
        "hit_id: %s\n" %(self.hit_id) +\
        "frame: %i %i\n" %(self.frame[0], self.frame[1])+\
        "hsp score: %.2f\n" %(self.hsp_score) +\
        "identities: %i\n" %(self.identities) +\
        "gaps: %i\n" %(self.gaps) +\
        "%% subject coverage: %.2f\n" %(self.frac_sbj_cov) +\
        "%% query coverage: %.2f\n" %(self.frac_query_cov) +\
        "%% alignment identity: %.2f\n" %(self.frac_aln_identities)
    # Called by repr() built-in function and by string
    # conversions (reverse quotes) to compute
    # the "official" string representation of an object.
    # If a class defines __repr__() but not __str__(),
    # then __repr__() is also used when an "informal"
    # string representation 
    def __repr__(self):
        return (self.application + u"\n" +\
                self.database + u"\n" +\
                self.blast_expect + u"\n" +\
                self.subject + u"\n" +\
                self.query + u"\n" +\
                self.query_length + u"\n" +\
                self.align_length + u"\n" +\
                self.expect + u"\n" +\
                self.query_start + u"\n" +\
                self.query_end + u"\n" +\
                self.sbjct_length + u"\n" +\
                self.sbjct_start + u"\n" +\
                self.sbjct_end + u"\n" +\
                self.hit_id + u"\n" +\
                self.frame + u"\n" +\
                self.hsp_score + u"\n" +\
                self.identities + u"\n" +\
                self.gaps + u"\n" +\
                self.frac_query_cov + u"\n" +\
                self.frac_sbj_cov + u"\n" +\
                self.frac_aln_identities)







# Explicitly pass command line arguments
# because required vor invoke main from
# other script
#
# It can receive as a parameter a list of
# Bio.Blast.Record objects
def main(arguments, blast_records = None):

    sys.argv = arguments
    
    args = arg_parse()

    # If is not passed a list of blast records object
    # as argument, it create such list parsing an xml
    # blast result file
    
    # The --xml blast_results.xml is required even if
    # the main function receives as input a list of Blast
    # Records objects . This is because the string in
    # args.blast_results[0] is used by function write_report()
    # to give a name to the search database
    if blast_records == None:
    
        #################
        blast_results = args.blast_results[0]
        # get blast results as input file
        try:
            blast_results_handle = open(blast_results)
        except:
            sys.exit("Could not open files %s") % (blast_results) 
        #################

        # parse blast XML and get blast records
        blast_records =  NCBIXML.parse(blast_results_handle)

 
    # evalue threshold score
    E_VALUE_THRESH = args.e_value_thresh
    QUERY_COV = args.query_cov
    ALN_IDENTITES = args.aln_identity
    IDENTITY = args.identity
    QUERY_ON_SBJ = args.query_on_sbj
    SBJ_ON_QUERY = args.sbj_on_query



    records_count, matching_queries  =  load_blast_hits(blast_records, \
                                                        E_VALUE_THRESH, \
                                                        QUERY_COV, \
                                                        ALN_IDENTITES, \
                                                        IDENTITY, \
                                                        QUERY_ON_SBJ, \
                                                        SBJ_ON_QUERY)

    
    # count matching records
    #matching_records = match_count(matching_queries)
    matching_records, report_file, = match_print(records_count, matching_queries, args)

    print u"Records number: %i\tWithout match: %i\n" %(records_count, records_count - matching_records)
    # return dictionary of matching query
    # for filter_blast_results.py
    return matching_queries





def load_blast_hits(blast_records, E_VALUE_THRESH, QUERY_COV, ALN_IDENTITES, \
                    IDENTITY, QUERY_ON_SBJ, SBJ_ON_QUERY):
    """
    Loads into memory only the blast record blast that exceed the filter values of
    Evalue, Query coverage, Alignment % coverage
    """
    records_count = int(0)
    hsps_count = int(0)
    # dictionary that save name of the quary as key
    # and True or False if it satisfies the filter conditions
    matching_queries = {}
    hit_dict = {}

    identity_match_a = {}
    
    query_on_sbj_match = {}
    sbj_on_query_match = {}
 
    for blast_record in blast_records:
        records_count +=1
                
        alignments_count = int(0)
        for alignment in blast_record.alignments:
            
            hsps_counts = int(0)
            for hsp in alignment.hsps:
                
                query_cov, sbj_cov, \
                           aln_identities, \
                           sbj_identities = coverages(blast_record,alignment,hsp)
                
                # filter against e-value
                if (hsp.expect <= E_VALUE_THRESH) and \
                       (query_cov >= QUERY_COV) and \
                       (aln_identities >= ALN_IDENTITES):

                    # create new Hit object and load information on it
                    new_blast_hit = Hit(blast_record.application,\
                                        blast_record.database,\
                                        blast_record.expect,\
                                        alignment.hit_def,\
                                        blast_record.query,\
                                        blast_record.query_length,\
                                        hsp.align_length,\
                                        hsp.expect,\
                                        hsp.query_start,\
                                        hsp.query_end,\
                                        alignment.length,\
                                        hsp.sbjct_start,\
                                        hsp.sbjct_end,\
                                        alignment.hit_id,\
                                        hsp.identities,\
                                        hsp.gaps,\
                                        hsp.frame,\
                                        hsp.score,\
                                        query_cov,\
                                        sbj_cov,\
                                        aln_identities)

                    # Use a dictionary to count the number of queries that meet the filter conditions.
                    # Use a dictionary is useful because even if the same quary is call because it
                    # shows several hsp march, it doesn't increase number of counts
                    # (the key is the same).
                    # key in dict

                    # I save for data of a new hsp only when it is included
                    # for the first time, or if its score > scores of the precedent hsp
                    # between same subject and query
                    if blast_record.query not in matching_queries:
                        matching_queries[blast_record.query] = new_blast_hit
                    elif hsp.score > matching_queries[blast_record.query].hsp_score:
                        matching_queries[blast_record.query] = new_blast_hit
                                                                      
                    hsps_counts +=1
                                     
                    # # print '****Record****'
                    # # print u"Query %% coverage: %.1f\tSubject %% coverage: %.1f\tAlignment %% identity %.1f:" %(query_cov, sbj_cov, aln_identities)
                    # # name of the query
                    # # print 'records query (query name):', blast_record.query
                    # # length of the query
                    # print 'records query_length:', blast_record.query_length
                    # # name of the subject
                    # # print 'alignments hit_def (subject name):', alignment.hit_def
                    # # length of the subject
                    # # print 'alignments length (subject length):', alignment.length
                    # # length of the alignment
                    # # For BLASTN alignment length is >= to the length of the shorter sequence
                    # # because it takes into account also the number of gaps inserted in the subject
                    # # and in the query.
                    # # print 'hsp align_length:', hsp.align_length
                    # # expectation value of the hsp
                    # # print 'hsp expect:', hsp.expect
                    # # point of alignment start on the query
                    # # print 'hsp query_start:', hsp.query_start
                    # # point of alignment stop on the query
                    # # print "hsp query_end:", hsp.query_end
                    # # number of identical position in the alignment
                    # # between subject and quary
                    # #print "hsp identities:", hsp.identities
                    # # start point of subject alignments
                    # # print "subject_start:", hsp.sbjct_start
                    # # end point of subject alignments
                    # # print "subject_end:", hsp.sbjct_end 
                    # # accession
                    # # print "alignment accession:", alignment.accession
                    # # database name
                    # # print "database_name: %s" %(os.path.basename(blast_record.database))
                    # # print "frame:", hsp.frame
                    # # Number of gaps (int) if using the XML parser.
                    # print "gaps:", hsp.gaps
                    # # The query sequence that align
                    # # print "Query sequence:\n", hsp.query
                    # #print "Length:", len(hsp.query)
                    # # The match sequence
                    # # print "Match:\n", hsp.match
                    # #print "Length:", len(hsp.match)
                    # # The sbjct sequence that align
                    # # print "Sbjct sequence:\n", hsp.sbjct
                    # #print "Length:", len(hsp.sbjct)
                    # print "\n"
                    # Blast score
                    #print "Blast score: ", hsp.score, "\n"
                    # print "Database sequences: ", blast_record.database_sequences, "\n"
                    # print "Database description: ","\n"
                    # for hit in blast_record.descriptions:
                        # print hit, "\n"
                    
                    # # print '****Record****'

            #print hsps_counts, "\n"

        #if records_count == 200:
            #break
        
    return records_count, matching_queries








def coverages(blast_rec,blast_aln,blast_hsp):
    """
    Calculate query and subject % of alignment coverages. Calculate % of alignments idetity
    """
    # % of query covered by the alignments
    
    # I cannot use the alignment.length, as the length of the alignment because it is given by
    # the number of identical + number of gaps opened in the subject and in the query.
    # I use the start and end points of the alignment on query normalizzed
    # math.fabs() return absolute value
    query_cov = math.fabs(int(blast_hsp.query_end) - int(blast_hsp.query_start))/ float(blast_rec.query_length) *100
    # % of subject covered by the alignments

    # As above I here use the start and end points of the alignment on subject normalizzed
    sbj_cov = math.fabs(int(blast_hsp.sbjct_end) - int(blast_hsp.sbjct_start))/ float(blast_aln.length) *100
    # % of identity along the alignments
    aln_identities = float(blast_hsp.identities) / float(blast_hsp.align_length) *100
    # % of identity along the subject
    sbj_identities = float(blast_hsp.identities) / float(blast_aln.length) *100
    return query_cov, sbj_cov, aln_identities, sbj_identities


  





def match_count(matching_queries):
    """
    Count query that satisfies the filter conditions
    """
    matching_records = int(0)
    for query in matching_queries.keys():
        matching_records +=1

    return matching_records





def match_print(records_count, matching_queries, args):
    """
    Print query that satisfies the filter conditions
    """    
    matching_records = int(0)
    
    report_items = []

    application = str()

    blast_expect = str()

    report_file = str()

    current_dir = os.getcwd()
    # create tmp dir
    tmp_dir = os.path.join(current_dir, "tmp")
    if (os.access(tmp_dir, os.F_OK) == False):
        os.mkdir(tmp_dir)
    else:
        shutil.rmtree(tmp_dir)
        os.mkdir(tmp_dir)
    os.chdir(tmp_dir)   
    
    for query in matching_queries.keys():

        matching_records +=1

        hit = matching_queries[query]

        application = hit.application

        blast_expect = u"%.2e" % hit.blast_expect
        
        # format the output string
        print u"%s,%i,%.2f,%s,%i,%.2f,%.2e,%i,%.2f" %(hit.query, \
                                                      hit.query_length, \
                                                      hit.frac_query_cov, \
                                                      hit.subject, \
                                                      hit.sbjct_length, \
                                                      hit.frac_sbj_cov, \
                                                      hit.expect, \
                                                      hit.align_length, \
                                                      hit.frac_aln_identities\
                                                      )

        sbj_on_query_diagram = diagram_composer(hit.query, hit.query_length, \
                                                [hit.query_start, hit.query_end], \
                                                hit.subject, hit.frame)
        
        #print "query:", match.query_start, match.query_end, match.query_length
        #print "subject:", match.sbjct_start, match.sbjct_end, match.sbjct_length

        query_on_sbj_diagram = diagram_composer(hit.subject, hit.sbjct_length, \
                                                [hit.sbjct_start, hit.sbjct_end], \
                                                hit.query, hit.frame)

        report_item = report_item_composer(sbj_on_query_diagram, query_on_sbj_diagram, hit)

        report_items.append(report_item)

    # print report file only
    # if some hit has been found
    if matching_records > 0:

        report_file = write_report(args, blast_expect, report_items, application, blast_expect, \
                                   records_count, matching_records)

        command = "pdflatex -interaction=nonstopmode -output-directory=%s %s" %(current_dir, report_file)
        # subprocess.Popen() module allows you to spawn new processes
	
        # shell=True: the specified command will be executed through the shell
        # args: string, it specifies the command string to execute through the shell
        # bufsize=-1: means to use the system default, which usually means fully buffered
        # stdin, stdout and stderr: specify the executed programs standard input, standard
        #                           output and standard error file handles, respectively
        # subprocess.STDOUT: indicates that standard error should go into shell standard output
        # subprocess.PIPE: indicates that a new pipe to the child should be created and file handle
        #                  will be returned
        p = subprocess.Popen(args=command, shell=True, bufsize=-1, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        # Popen.comunicate(input): interact with process: Send data to stdin.
        # Read data from stdout and stderr, as files until end-of-file is reached.
        # ## Wait for process to terminate ##
        # returns a tuple of (stdoutdata, stderrdata) - Not silgle lines
        p.communicate(input=None)

    os.chdir(current_dir)
        
    return matching_records, os.path.abspath(report_file)








def diagram_composer(reference_name = u"", reference_length = int(0) , hit_1_aln = [0,0], hit_1_name = u"", hit_1_frame = (0,0), hit_2_aln = [0,0], hit_2_name = u"", hit_2_frame = (0,0)):
    """
    Compose diagram from one blast alignment
    """
    # color of the feature from hit 1
    color_1 = colors.purple
    # color of the feature from hit 2
    color_2 = colors.blue

    #diagram_composer_test(reference_name, reference_length, hit_1_aln, hit_1_name, hit_2_aln, hit_2_name)

    # Creation of the diagram object. Need the name of the reference
    # A diagram object represents a sequence (or sequence region)
    # along the horizontal axis
    gd_diagram = GenomeDiagram.Diagram(reference_name)
    # Creation of empty track. A diagram can contain one or more tracks,
    # shown stacked vertically. These will all have the same length and
    # represents the same sequence region.
    gd_track_for_features = gd_diagram.new_track(1, name=u"Alignments on %s" %(reference_name), greytrack=False)
    # Creation of an empty feature set. Track will contain features,
    # bundled together in feature-sets. It makes it easier to update
    # the properties of just selected features
    gd_feature_set = gd_track_for_features.new_set()


    # Tests whether the alignment between query sequence and subject is in forward or reverse
    # strand.
    # hit_frame: tuple of blastrecord.hsp.frame(-1,2)
    # hsp.frame: tuple of 1 or 2 frame shifts, depending on the flavor.
    # If fram(0) and frame(1) have opposite signs, then the alignment between subject and query
    # is done on forward strand; if they have the same sign instead, it is done on the reverse strand.
    strand, label_angle = get_strand(hit_1_frame)

    
    # Features are SeqRecord's SeqFeature objects. You have to create
    # minimal SeqFeature object (i.e. just the coordinates for a feature you want)
    # for add a new feature.
    # strand: +1 for the forward strand,
    #         -1 for the reverse strand,
    #         None for both
    #         You can not define the strand alignments performed with "blastx" and "tbalstx.
    #         You can only define the strand of alignments performed with "blanstn.
    feature_1 = SeqFeature(FeatureLocation(hit_1_aln[0], hit_1_aln[1]), strand=strand)
    # Add a new feature:
    # sigil: arrow shape "ARROW" "BOX"
    # arrowshaft_height: the thickness of the arrow shaft, given as a proportion
    #                    of the height of the bounding box
    # arrowhead_length: the length of the arrow head - given as a proportion of
    #                   the height of the bounding box
    # track_size:   float, the proportion of the space available to each 
    #                      track that should be used in drawing
    
    # I can not get information on the directionality of the arrays, as the numbers start and
    # end alignment on the subject is that the query are listed in sequential order even though
    # the sequences are aligned in reverse order. For this I use sigil="BOX" instead of "ARROW"
    gd_feature_set.add_feature(feature_1, color=color_1, label=True, name=hit_1_name, \
                               label_size=10, label_angle=label_angle, sigil="ARROW", \
                               label_color="green",\
                               label_position="start", \
                               arrowhead_length=0.8, \
                               arrowshaft_height=0.5)
    # see above
    strand, label_angle = get_strand(hit_2_frame)
    feature_1 = SeqFeature(FeatureLocation(hit_2_aln[0], hit_2_aln[1]), strand=strand)
    gd_feature_set.add_feature(feature_1, color=color_2, label=True, name=hit_2_name, \
                               label_size=10, label_angle=label_angle, sigil="ARROW", \
                               label_color="green",\
                               label_position="end", \
                               arrowhead_length=0.8, \
                               arrowshaft_height=0.5)

    # Creates all the shapes using ReportLab objects
    gd_diagram.draw(format='linear', pagesize=(20*cm,8*cm), fragments=1,
                 start=0, end=reference_length, xr = 0.3, fragment_size=0.5)
    
    # Out file name
    out_file = u"%s_graph.pdf" %(rand1(5))
    
    # Renders the shapes to the requested format. You can output in multiple formats:
    # "PDF"
    # "EPS"
    # "SVG"
    # "PNG" (get errors!)
    gd_diagram.write(out_file, "pdf")
    
    return os.path.abspath(out_file)



def get_strand(hit_frame):
    """
    Tests whether the alignment between query sequence and subject is in forward or reverse
    strand.
    hit_frame: tuple of blastrecord.hsp.frame(-1,2)
    hsp.frame: tuple of 1 or 2 frame shifts, depending on the flavor.
    If fram(0) and frame(1) have opposite signs, then the alignment between subject and query is done
    on forward strand; if they have the same sign instead, it is done on the reverse strand.
    """
    strand = int(+1)
    label_angle = 10
    
    # For blastx (and probably tblastx) the tuple contains
    # two values (each positive or negative) other than 0
    if hit_frame[0] != 0 and hit_frame[1] != 0:
        # if reverse strand
        if (hit_frame[0] * hit_frame[1]) < 0:
            strand = int(-1)
            label_angle = 190

    # For tblastn the tuple contains one
    # value other than 0 (positive or negative)
    # and a the other equal to 0
    # So I do a test on the value other than 0
    elif hit_frame[0] == 0:
        # if reverse strand
        if hit_frame[1] < 0:
            strand = int(-1)
            label_angle = 190
            
    elif hit_frame[1] == 0:
        # if reverse strand
        if hit_frame[0] < 0:
            strand = int(-1)
            label_angle = 190

    return strand, label_angle




# \\item 
#    \\begin{enumerate}
#        \\end{enumerate}
def report_item_composer(subject_diagram_file, query_diagram_file, hit):
    """
    Composes an item for the main source of the subtraction Latex report.
    Each item represents the information and graphics related to a common match between two query of
    the libraries compared
    """
    report_item =\
u"""
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  \\item
    {
    hit id:
    \\begin{verbatim}%s\\end{verbatim}
    subject:
    \\begin{verbatim}%s\\end{verbatim}
    subject length: %i

    Searched database:
    \\begin{verbatim}%s\\end{verbatim}
    e-value: %.2e

    query:
    \\begin{verbatim}%s\\end{verbatim}
    alignment length: %i

    \\%% alignment identities: %.2f

    query start: %i 

    query end: %i

    \\%% query coverage: %.2f

    subject start: %i

    subject end: %i

    \\%% subject coverage: %.2f
    

		
    \\medskip
    \\begin{figure}[h!]
    \\begin{center}
    \\includegraphics[width=150mm]{%s}
    \\end{center}
    \\caption[]{Alignment of the query on the subject %s}
    \\end{figure}

    \\begin{figure}[h!]
    \\begin{center}
    \\includegraphics[width=150mm]{%s}
    \\end{center}
    \\caption[]{Alignment of the subject on the query %s}
    \\end{figure}
    }

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
""" %(
      hit.subject, \
      hit.hit_id, \
      hit.sbjct_length, \
#tbp
      hit.database, \
      hit.expect, \
      hit.query.replace("_", u"\\_"), \
      hit.align_length, \
      hit.frac_aln_identities, \
      hit.query_start, \
      hit.query_end, \
      hit.frac_query_cov, \
      hit.sbjct_start, \
      hit.sbjct_end, \
      hit.frac_sbj_cov, \

      query_diagram_file, \
      hit.subject.replace("_", u"\\_"), \
      subject_diagram_file, \
      hit.query.replace("_", u"\\_"), \
)
    return report_item







def write_report(args, expect, report_items, application, blast_expect, records_count, matching_records):
    """
    Compose the main Latex source of the subtraction report
    """
    report_handle = open(args.report_file + r".tex", "w")
    report_text =\
u"""
\\documentclass[titlepage]{report}
\\usepackage{graphicx}
\\begin{document}
\\title{Subtraction Libraries Report}
\\noindent



\\chapter*{BLAST statistics}
\\noindent

    Blast search results for chunk: \\verb|%s|

    from results file: \\verb|%s|
    
    Total records count in the chunk: %i
    
    
    without match: %i
    \\medskip

    The similarity searches were made with \\textbf{%s}


    with expect %s.


    E-value filter threshold: %.2e
    
    
    Query coverage threshold: %.2f
    

    Alignment identity threshold: %.2f

\\medskip
\\begin{enumerate}
  %s
\\end{enumerate}

\\end{document}
"""%(args.report_file, \
     args.blast_results[0], \
     records_count, \
     records_count - matching_records, \
     application,\
     expect, \
     args.e_value_thresh,\
     args.query_cov,\
     args.aln_identity,\
     """

     """.join(report_items)
)

    
    report_handle.write(report_text)
    report_handle.close()
    return os.path.abspath(report_handle.name)




def rand1(leng):
    """
    Generate a random string of characters
    and fixed length = leng
    """
    nbits = leng * 6 + 1
    bits = random.getrandbits(nbits)
    uc = u"%0x" % bits
    newlen = int(len(uc) / 2) * 2 # we have to make the string an even length
    ba = bytearray.fromhex(uc[:newlen])
    return base64.urlsafe_b64encode(str(ba))[:leng]




def arg_parse():
    """
    Parses command line arguments
    """
    parser = argparse.ArgumentParser(description="Get difference between blast results")
    parser.add_argument("--xmls", "-x", dest="blast_results", nargs='+', required=True, help="Blast results in xml formats")
    parser.add_argument("--eval", "-e", dest="e_value_thresh", default=0, required=False, type=float, help="E-value threshold")
    parser.add_argument("--query_cov", "-q", dest="query_cov", default=0, required=False, type=float, help="% query coverage threshold")
    parser.add_argument("--aln_idn", "-a", dest="aln_identity", default=0, required=False, type=float, help="% alignment identity threshold")
    
    parser.add_argument("--id", "-i", dest="identity", action='store_false', default=True, required=False, help="filters mutual match")
    parser.add_argument("--qos", "-s", dest="query_on_sbj", action='store_false', default=True, required=False, help="filters match of query contained in the subject")
    parser.add_argument("--soq", "-o", dest="sbj_on_query", action='store_false', default=True, required=False, help="filters match of subject contained in the query")
    
    parser.add_argument("--rep_file", "-r", dest="report_file", default="report", required=False, help="name of the report file")
    args = parser.parse_args()
    
    return args



# Is invoked when the module is executed
if __name__ == "__main__":
    # Explicitly pass command line arguments
    # because required vor invoke main from
    # other script
    main(sys.argv)



# 
# blast2filter.py ends here
