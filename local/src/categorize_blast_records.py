# categorize_blast_records.py --- 
# 
# Filename: categorize_blast_records.py
# Description: 
# Author: Michele
# Maintainer: 
# Created: Mon Jun 13 12:52:58 2011 (+0200)
# Version: 
# Last-Updated: 
#           By: 
#     Update #: 0
# URL: 
# Keywords: 
# Compatibility: 
# 
# 

# Commentary: 
# 
# 
# 
# 

# Change Log:
# 
# 
# 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth
# Floor, Boston, MA 02110-1301, USA.
# 
# 

# Code:
from Bio.Blast import NCBIXML
# stores information about one hit in the alignments section
from Bio.Blast import Record as BlastRecord
# utility modules
import sys,os,argparse


def main(arguments):
    sys.argv = arguments
    
    args = arg_parse()
    
    # blast record to be splitted
    blast_results = args.blast_results

    # sequence list file name
    seq_lsts = []
    file_names = []
    
    for file_lst in args.file_lsts:
        # charge contig list, one name per line
        # on list
        seq_lsts.append(load_seq_lst(file_lst))

        # As the names of files that contains lists of contigs are
        # used as keys to record Blast Records chunks, I extract
        # these names and their extensions. The name is given in
        # the form of absolute path, so you have to Extract the
        # relative path
        file_name, extension = get_file_name(file_lst)
        file_names.append(file_name)
    
    #################
    # get blast results as input file
    try:
        blast_results_handle = open(blast_results)
    except:
        sys.exit("Could not open files %s") % (blast_results) 
    #################

    # parse blast XML and get blast records
    blast_records =  NCBIXML.parse(blast_results_handle)

    # Divide the Blast records in three lists, depending on the name of the subjects
    blast_record_chunks = categorize_blast_records(blast_records, seq_lsts, file_names)
    
    return blast_record_chunks




def get_file_name(name_extension):
    """
    Split full file_name.extension in file_name and
    extension
    """
    extension = str()
    file_name = str()
    basename = os.path.basename(name_extension)
    # get extension starting from end
    extension = basename.split(r".")[-1]
    # get file name as the rest of list other than
    # extension
    file_name = ".".join(basename.split(r".")[:-1])
    
    return file_name, extension



def load_seq_lst(file_name):
    """
    Parses a list of names of sequences in text format.
    The file must have no header and the sequence name
    must be the first word of each line
    """
    seq_lst = []
    for line in open(file_name, "r"):
        # take first word of the line and
        # put it on a list
        seq_lst.append(line.split()[0])
    return seq_lst




# Cleanest Python find-in-list function
def index(seq, item):
    """Return the index of the first item in seq where seq[i] == item.
    If no item is found return None"""
    return next((i for i in range(len(seq)) if seq[i] == item), None)
            



def categorize_blast_records_2(blast_records, seq_lsts, file_names):
    """
    Divide the Blast records in three lists, depending on the name of the subjects.
    The names of the subjects are given in two lists, in the third category ends up
    records with subject that are not included in the first, nor in the second list.
    Return a dictionary of keys = file_name_lst, value = chunk list

    Less efficient
    """
    blast_record_chunks = {}
    
    # Bio.Blast.NCBIXML returns an iterator a
    # Blast record for each query. The intention of the
    # itaration protocol is that once an iterators next()
    # method raises StopIteration, it will continue to do
    # so on subsequent calls.

    # So, if I want to iterate over the same Bio.Blast.Record,
    # I must first convert the iterator into a list
    blast_records_lst = list(blast_records)

    for i in range (len(seq_lsts)):

        blast_record_chunk = []

        for blast_record in blast_records_lst:
        
            for alignment in blast_record.alignments:


                find_flag = index(seq_lsts[i], str((alignment.hit_def)))

                # Bio.Blast.Record focuses on the query. Since the query can have a significant
                # match with various Subjects, Bio.Blast.Record.Alignment each object contains
                # information relating to an alignment. For each Bio.Blast.Record all arrays are
                # stored in the list Bio.Blast.Record.Alignments. It is possible that each
                # Bio.Blast.Record.Alignment relates to a subject that belongs to a different list.
                # I wants to create a new Bio.Blast.Record with the same data as the overall,
                # but the list Bio.Blast.Record.Alignments now will contain only the current
                # Bio.Blast.Record.Alignment object. This new Bio.Blast.Record will be saved
                # in the corresponding chunk in accordance with the subject to which it is related
                sigle_align_Brecord = get_sigle_align_Brecord(blast_record, alignment)

                # select which category the subject of the blast record is
                if (find_flag != None):
                    blast_record_chunk.append(sigle_align_Brecord)

        blast_record_chunks[file_names[i]] = blast_record_chunk
                
    # return a dictionary of
    # keys = file_name_lst, value = chunk list             
    return blast_record_chunks



def categorize_blast_records(blast_records, seq_lsts, file_names):
    """
    Divide the Blast records in three lists, depending on the name of the subjects.
    The names of the subjects are given in two lists, in the third category ends up
    records with subject that are not included in the first, nor in the second list.
    Return a dictionary of keys = file_name_lst, value = chunk list

    More efficient
    """
    blast_record_chunks = {}

    # prepare dictionary of empty chunk lists
    for i in range (len(seq_lsts)):
        blast_record_chunk = []
        blast_record_chunks[file_names[i]] = blast_record_chunk
        
    # So, if I want to iterate over the same Bio.Blast.Record,
    # I must first convert the iterator into a list
 
    for blast_record in blast_records:

        for alignment in blast_record.alignments:

            for i in range(len(seq_lsts)):

                find_flag = index(seq_lsts[i], str((alignment.hit_def)))

                # Bio.Blast.Record focuses on the query. Since the query can have a significant
                # match with various Subjects, Bio.Blast.Record.Alignment each object contains
                # information relating to an alignment. For each Bio.Blast.Record all arrays are
                # stored in the list Bio.Blast.Record.Alignments. It is possible that each
                # Bio.Blast.Record.Alignment relates to a subject that belongs to a different list.
                # I wants to create a new Bio.Blast.Record with the same data as the overall,
                # but the list Bio.Blast.Record.Alignments now will contain only the current
                # Bio.Blast.Record.Alignment object. This new Bio.Blast.Record will be saved
                # in the corresponding chunk in accordance with the subject to which it is related
                sigle_align_Brecord = get_sigle_align_Brecord(blast_record, alignment)

                # select which category the subject of the blast record is
                if (find_flag != None):
                    blast_record_chunks[file_names[i]].append(sigle_align_Brecord)
                
    # return a dictionary of
    # keys = file_name_lst, value = chunk list             
    return blast_record_chunks





def get_sigle_align_Brecord(blast_record = BlastRecord, alignment = BlastRecord.Alignment):
    """
    Given an Bio.Blast.Record object and an Bio.Blast.Record.Alignment object,
    produces a new Bio.Blast.Record identical to the pass one, but with the list
    Bio.Blast.Record.Alignments that contains only the object Bio.Blast.Record.Alignment
    passed
    """
    # empty new Bio.Blast.Record
    sigle_align_Brecord = BlastRecord
    # copy passed Bio.Blast.Record to new one
    sigle_align_Brecord = blast_record
    # empty the Bio.Blast.Record.Alignments list
    del sigle_align_Brecord.alignments[:]
    # append to list the Bio.Blast.Record.Alignment passed
    sigle_align_Brecord.alignments.append(alignment)

    return sigle_align_Brecord



def arg_parse():
    """
    Parses command line arguments
    """
    parser = argparse.ArgumentParser(description="Divides Bio.Blast.Record-s according to subject sequences listed in various files. Lists must have one sequence name per line. Is not carried out any checks if a name of a sequence is present in more than one list.")
    parser.add_argument("--xml", "-x", dest="blast_results", required=True, help="Blast results in xml format")
    parser.add_argument("--lsts", "-l", dest="file_lsts", nargs='+', required=True, help="Lists of contigs in text format")

    args = parser.parse_args()
    
    return args




# Is invoked when the module is executed
if __name__ == "__main__":
    main(sys.argv)

# 
# categorize_blast_records.py ends here
