# test_blast2filter.py --- 
# 
# Filename: test_blast2filter.py
# Description: 
# Author: Michele
# Maintainer: 
# Created: Mon Jun 13 16:26:25 2011 (+0200)
# Version: 
# Last-Updated: 
#           By: 
#     Update #: 0
# URL: 
# Keywords: 
# Compatibility: 
# 
# 

# Commentary: 
# 
# 
# 
# 

# Change Log:
# 
# 
# 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth
# Floor, Boston, MA 02110-1301, USA.
# 
# 

# Code:
from Bio.Blast import NCBIXML
# argument parser
import argparse
#
import blast2filter

# Explicitly pass command line arguments
# because required vor invoke main from
# other script
def main():
    
    args = arg_parse()

    #################
    blast_results = args.blast_results[0]
    # get blast results as input file
    try:
        blast_results_handle = open(blast_results)
    except:
        sys.exit("Could not open files %s") % (blast_results) 
    #################

    # parse blast XML and get blast records
    blast_records =  NCBIXML.parse(blast_results_handle)

    # thresholds
    E_VALUE_THRESH = float(10)
    QUERY_COV = float(50.0)

    xml = r"pippo.xml"

    # To call one python script from Within Another python script is enough:
    
    # import script name
    # script_name.main (command_line_arguments.split ())
    
    # as I pass command line argument list equal to the output of sys.argv
    
    # blast2cytoscape filters the BLAST results and produces a report file in
    # pdf format
    command = r"blast2filter --xmls %s --eval %.2e --query_cov %.2f --rep_file %s"\
              %(xml, E_VALUE_THRESH, QUERY_COV, xml[:-4])
    
    print command

    # I try to call the script in question and pass it two arguments:
    # a string of command line arguments and a list data structure
    matching_queries = blast2filter.main(command.split(), blast_records)

    return 0



def arg_parse():
    """
    Parses command line arguments
    """
    parser = argparse.ArgumentParser(description="get difference between blast results")
    parser.add_argument("--xmls", "-x", dest="blast_results", nargs='+', required=True, help="Blast results in xml formats")
    parser.add_argument("--eval", "-e", dest="e_value_thresh", default=0, required=False, type=float, help="E-value threshold")
    parser.add_argument("--query_cov", "-q", dest="query_cov", default=0, required=False, type=float, help="% query coverage threshold")
    parser.add_argument("--aln_idn", "-a", dest="aln_identity", default=0, required=False, type=float, help="% alignment identity threshold")
    
    parser.add_argument("--id", "-i", dest="identity", action='store_false', default=True, required=False, help="filters mutual match")
    parser.add_argument("--qos", "-s", dest="query_on_sbj", action='store_false', default=True, required=False, help="filters match of query contained in the subject")
    parser.add_argument("--soq", "-o", dest="sbj_on_query", action='store_false', default=True, required=False, help="filters match of subject contained in the query")
    
    parser.add_argument("--rep_file", "-r", dest="report_file", default="report", required=False, help="name of the report file")
    args = parser.parse_args()
    
    return args




# Is invoked when the module is executed
if __name__ == "__main__":
    main()

# 
# test_blast2filter.py ends here
