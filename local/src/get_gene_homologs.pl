# get_gene_homologs.pl --- 
# 
# Filename: get_gene_homologs.pl
# Description: 
# Author: Michele
# Maintainer: 
# Created: Tue May 24 17:30:46 2011 (+0200)
# Version: 
# Last-Updated: 
#           By: 
#     Update #: 0
# URL: 
# Keywords: 
# Compatibility: 
# 
# 

# Commentary: 
# 
# 
# 
# 

# Change Log:
# 
# 
# 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth
# Floor, Boston, MA 02110-1301, USA.
# 
# 

# Code:
use strict;
use warnings;
use Cwd;
use Cwd 'abs_path';
    

my @gene_lst = 
    (
     # Sex Determination in the Squalius alburnoides Complex: An Initial Characterization of Sex Cascade Elements in the Context of a Hybrid Polyploid Genome
     #'DMRT',
     'WT1',
     'DAX',
     #'FIGLA',
     # Discovery in sturgeon
     #'SOX',
     'RSPO',
     'WNT',
     'FOX',
     'TRA',
     'FEM',
     # Amh and Dmrta2 Genes Map to Tilapia (Oreochromis spp.) Linkage Group 23 Within Quantitative Trait Locus Regions for Sex Determination
     'SF',
     'CYP',
     'AMH',
     'FHL',
     'LHX',
     # The evolution of sex chromosomes and sex determination in vertebrates and the key role of DMRT1
     'GATA',
     'ATRX',
     'FGF',
     'TDA',
     'DHTR',
     'PAX',
     'LIM',
     'EMX',
     'INSL',
     'DHH'
    );

my $script = '/home/michele/Research/Projects/Transcriptomic_analysis/Results/cDNAstorione/2011-05-17/get_Homology_from_compara.1.pl';


foreach my $gene (@gene_lst)
{
    my $apex_wildcards_gene = '%'.$gene;
    my $pedix_wildcards_gene = $gene.'%';
    
    my ($nr_dir, $r_dir) = &create_dirs($apex_wildcards_gene, $script);
    my ($nr_dir, $r_dir) = &create_dirs($pedix_wildcards_gene, $script);
}


sub create_dirs
{
    my $gene = $_[0];
    my $script = $_[1];

    my $current_dir = getcwd;
    my $nr_dir = $gene."_gene_non_redundant";
    my $r_dir = $gene."_redundant";

    mkdir $nr_dir, 0755 or warn "Cannot make $nr_dir directory $!";

    chdir $nr_dir or die "cannot chdir to $nr_dir: $!";
    my ($r_out, $nr_out) = &do_in_dir($script, $gene, "yes");
    chdir $current_dir;

    mkdir $r_dir, 0755 or warn "Cannot make $r_dir directory $!";

    chdir $r_dir or die "cannot chdir to $r_dir: $!";
    my ($r_out, $nr_out) = &do_in_dir($script, $gene, "no");
    chdir $current_dir;
    
    return ($script, $r_dir);
}


sub do_in_dir
{
    my $script = $_[0];
    my $gene = $_[1];
    my $redundant = $_[2];

    my $r_out = "out_$gene"."_redundant.txt";
    my $nr_out = "out_$gene"."_non_redundant.txt";
    
    if ($redundant eq "yes")
    {
	system("perl $script --specie_db homo_sapiens --gene_name $gene --make_nr $redundant > $nr_out");
    }
    elsif ($redundant eq "no")
    {
    	system("perl $script --specie_db homo_sapiens --gene_name $gene --make_nr $redundant > $r_out");
    }

    return (abs_path($r_out), abs_path($nr_out));
}

# 
# get_gene_homologs.pl ends here
