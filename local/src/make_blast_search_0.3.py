# make_blast_search.py --- 
# 
# Filename: make_blast_search.py
# Description: 
# Author: Michele
# Maintainer: 
# Created: Thu Jun  2 13:14:33 2011 (+0200)
# Version: 
# Last-Updated: 
#           By: 
#     Update #: 0
# URL: 
# Keywords: 
# Compatibility: 
# 
# 

# Commentary: 
# 
# 
# 
# 

# Change Log:
# 
# 
# 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth
# Floor, Boston, MA 02110-1301, USA.
# 
# 

# Code:

# Code:
from blast_tools.NCBI_BLAST import NCBIBLAST
import sys,os,time



def main():

    gene_lst = [     # Sex Determination in the Squalius alburnoides Complex: An Initial Characterization of Sex Cascade Elements in the Context of a Hybrid Polyploid Genome
     r'DMRT',\
     r'WT1',\
     r'DAX',\
     r'FIGLA',\
     # Discovery in sturgeon
     r'SOX',\
     r'RSPO',\
     r'WNT',\
     r'FOX',\
     r'TRA',\
     r'FEM',\
     # Amh and Dmrta2 Genes Map to Tilapia (Oreochromis spp.) Linkage Group 23 Within Quantitative Trait Locus Regions for Sex Determination
     # r'SF',\
     r'CYP',\
     r'AMH',\
     r'FHL',\
     r'LHX',\
     # The evolution of sex chromosomes and sex determination in vertebrates and the key role of DMRT1
     r'GATA',\
     r'ATRX',\
     r'FGF',\
     r'TDA',\
     r'DHTR',\
     r'PAX',\
     r'LIM',\
     r'EMX',\
     r'INSL',\
     r'DHH'\
        ]
    
    # if True only delete
    # precedent Blast results
    # in xml format
    # if False generates
    # new results
    delete = False

    current_dir = os.getcwd()
    blast_db_lst = get_blast_DB_lst(current_dir)


    for gene in gene_lst:

        dir_name = gene + r"_redundant"

        if os.path.isdir(dir_name) ==  True:
            os.chdir(dir_name)

        dir_lst = os.listdir(u".")

        for file_name in dir_lst:
            
            if len(file_name.split(r".")) >= 2:
                # get extension starting from end
                extension = file_name.split(r".")[-1]

                if (delete == True):

                    # remove precedent blast results files
                    # N.B.: if you use it to delete all the 
                    # previously xml products, remember to
                    # comment these lines for avoid to delete
                    # xmls when searching by BLAST They come
                    if extension == r"xml":
                        os.remove(file_name)
                else:
                    # perform blast search
                    if extension == r"fasta":
                        # get file name as the rest of list other than
                        # extension
                        gene_name = ".".join(file_name.split(r".")[:-1])

                        for blast_DB in blast_db_lst:
                            results_xmls_lst = search_blast_DB(gene_name, extension, blast_DB)
                   
        os.chdir(current_dir)

    return 0








def get_blast_DB_lst(current_dir):
    """
    Get the name of the blast database in the current directory
    from the name of the corresponding fasta from witch database
    was generated
    """

    blast_db_lst = []

    dir_lst = os.listdir(u".")

    for file_name in dir_lst:

        if len(file_name.split(r".")) >= 2:
            # get extension starting from end
            extension = file_name.split(r".")[-1]

            # remove precedent blast db files
            # if extension != r"fasta" and extension != r"txt":
                # os.remove(file_name)

            if extension == r"fasta":
                # get file name as the rest of list other than
                # extension
                file_name = ".".join(file_name.split(r".")[:-1])
                # add absolute path to database name
                blast_db_lst.append(os.path.join(current_dir, file_name))

    return blast_db_lst






def search_blast_DB(query_name, query_extension, db):
    """
    Search in the Blast database
    """
    
    # set out blast database 
    out_file = u""
    
    # compose query name
    in_query = (r"%s.%s") %(query_name, query_extension)

    # get basename of the database sequence
    db_basename = os.path.basename(db)

    # iterate on all evalue list
    evalues_lst = [10]#, 1e-06, 1e-10, 1e-20, 1e-50]

    results_xmls_lst = []

    for evalue in evalues_lst:
        
        # set blast results xml file
        xml_file = u"%s_%s_%s_%s_redundant.xml" %(db_basename.split(r"_")[0], db_basename.split(r"_")[-1], query_name, str(evalue))

        # create new object
        ncbiblast = NCBIBLAST()
    
        # search on previously created db
        ncbiblast.set_blast_bin(r"tblastn")
        ncbiblast.set_db(db)
        ncbiblast.set_quary(in_query)
        ncbiblast.set_out_file(xml_file)
        # set options for blast search
        common_options = [r"-evalue %s" % evalue, r"-num_threads 4", r"-outfmt 5", r"-max_target_seqs 20"]
        prog_specific_options = [r""]
        print ncbiblast.set_blast_settings(common_options, prog_specific_options)
        ncbiblast.search()
        results_xmls_lst.append(xml_file)

    return results_xmls_lst




if __name__ == "__main__":
    main()

# 
# make_blast_search.py ends here
