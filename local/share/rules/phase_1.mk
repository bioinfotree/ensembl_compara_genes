### phase_1.mk --- 
## 
## Filename: phase_1.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Wed Aug  3 15:24:25 2011 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:

PRJ ?=

PRJ_PREFIX ?=

context prj/annotation

# link to CDNA3 (male library)
extern ../../../../library_substraction/dataset/$(PRJ)/phase_4/CDNA1.fasta as CDNA1_UNIQ_FASTA
# link to CDNA4 (female library)
extern ../../../../library_substraction/dataset/$(PRJ)/phase_4/CDNA2.fasta as CDNA2_UNIQ_FASTA
# link to COVERAGE table of final assembly
extern ../../../../454_assembly/dataset/$(PRJ)/phase_3/$(PRJ_PREFIX)_1_as_unpad.fasta_stats.txt as ASSEMBLY_1_COV

# gene list to search for
GENES ?=
# specie in which first search for genes
SPECIE ?=
# cdna to be searched for homologues
CDNA ?=
# version of ensembl databases to be used
ENSEMBL_VER ?= 66
# e-value threshold for tblastn
BLAST_EVAL ?= 0.001
# e-value threshold for blast_fields
FILTER_EVAL ?= 1e-06
# fraction query coverage for blast_fields [0.0-1.0]
QUERY_COV ?= 0.5
# miminum number of positive residues in the hsp
MIN_POS ?=
# if GENES are protein sequence (default) or nucleotide sequences
# "nuc" or "prot"
QUERY_TYPE ?= prot



cdna.fasta:
	ln -sf $(CDNA) $@


SEQUENCES = $(addsuffix .fasta, $(GENES))

#remove ; from fasta headers because I use it later as separator
%.fasta:
	if [ -s $@ ]; then \
	touch $@; \
	else \
	rm -rf $@; \
	with_ensembl_api $(ENSEMBL_VER) get_homology_from_compara_gene --gene_name $*% --specie $(SPECIE) > $@; \
	fi; \
	sed -i 's/;/(/g' $@; \
	sed -i 's/,//g' $@;



LENGTHS = $(addsuffix .len, $(GENES))
%.len: %.fasta
	if [ -s $< ]; then \
	fasta_length < $< \
	| stat_base --mean --median --stdev --min --max 2 \
	| sed 1'i\#mean\tmedian\tstdv\tmin\tmax' > $@; \
	fi

cdna.flag: cdna.fasta
	_comment () { echo -n ""; }; \
	mkdir -p $(basename $@); \
	cd $(basename $@); \
	ln -sf ../$<; \
	makeblastdb -in $< -dbtype nucl -title $(basename $@) -out $(basename $@); \
	cd ..; \
	touch $@



BLAST_RESULTS = $(addsuffix .xml, $(GENES))
%.xml: cdna.flag %.fasta
ifeq ($(QUERY_TYPE),nuc)
	!threads
	if [ -s $^2 -a ! -s $@ ]; then \
	blastn -evalue $(BLAST_EVAL) -num_threads $$THREADNUM -outfmt 5 -max_target_seqs 5 -query $^2 -db $(basename $<)/$(basename $<) -out $@; \
	fi
else
	!threads
	if [ -s $^2 -a ! -s $@ ]; then \
	tblastn -evalue $(BLAST_EVAL) -num_threads $$THREADNUM -outfmt 5 -max_target_seqs 5 -query $^2 -db $(basename $<)/$(basename $<) -out $@; \
	fi
endif



BLAST_FILTERED = $(addsuffix .tab, $(GENES))
%.tab: %.xml
	if [ -s $< ]; then \
	blast_fields -f -r query -a hit_def -p expect,bits,positives,sbjct,sbjct_start,sbjct_end -e $(FILTER_EVAL) -m $(MIN_POS) < $< > $@; \
	fi


cdna1_uniq.lst: $(CDNA1_UNIQ_FASTA)
	fasta2tab <$< | cut -f 1 > $@

cdna2_uniq.lst: $(CDNA2_UNIQ_FASTA)
	fasta2tab <$< | cut -f 1 > $@

cdna_comm.lst: $(CDNA) cdna1_uniq.lst cdna2_uniq.lst
	comm -2 -3 <(fasta2tab <$< | cut -f 1 | bsort) \
	<(cat $(call rest,$^) | bsort) > $@



BLAST_CDNA1 = $(addsuffix _cdna1.tab, $(GENES))
%_cdna1.tab: %.tab cdna1_uniq.lst
	$(call translate_lst,$<,$^2,$@)

BLAST_CDNA2 = $(addsuffix _cdna2.tab, $(GENES))
%_cdna2.tab: %.tab cdna2_uniq.lst
	$(call translate_lst,$<,$^2,$@)

BLAST_COMM = $(addsuffix _comm.tab, $(GENES))
%_comm.tab: %.tab cdna_comm.lst
	$(call translate_lst,$<,$^2,$@)

# translate options:
# 	-f 2: use column 2 as key field for the dictionary
# 	1:    translate column 1 of tab
# bsort options:
#	It sorts first on field 1 with -b mode, and then on field 3 with -g mode
#	where field 1 is the same.
# awk:
#	join columns other the the first, with ","
# collapsesets:
#	collapse values in joned columns on the basis of column 1
define translate_lst
	if [ -s $1 ]; then \
	paste <(cut -f 2 <$1) <(cut --complement --output-delimiter="," -f 2 <$1) \
	| translate -a -d -k -g ";" -f 1 <(cat -) 1 < $2 \
	| expandsets --separator ";" 2 \
	| tr "," \\t \
	| bsort --key=1,1b --key=3,3g \
	| awk -F'\t' '!/^[$$]/ { \
	printf "%s\t",$$1; \
	for(i=2; i<NF; i++) { printf("%s,",$$i); } \
	printf $$NF"\n"; \
	}' \
	| collapsesets --preserve-order 2 > $3; \
	else \
	> $3; \
	fi
endef


summary_$(ENSEMBL_VER)_$(FILTER_EVAL)_$(MIN_POS).tab: $(BLAST_CDNA1) $(BLAST_CDNA2) $(BLAST_COMM)
	printf "gene_name\tcdna1\tcdna2\tcommon\n"> $@; \
	for GENE in $(GENES); do \
	CDNA1_COUNT=`wc -l <$$GENE\_cdna1.tab`; \
	CDNA2_COUNT=`wc -l <$$GENE\_cdna2.tab`; \
	COMMON=`wc -l <$$GENE\_comm.tab`; \
	printf "%s\t%i\t%i\t%i\n" "$$GENE" "$$CDNA1_COUNT" "$$CDNA2_COUNT" "$$COMMON" >> $@; \
	done



SBJ_ALN = $(addsuffix _sbj.aln, $(GENES))
%_sbj.aln: %_cdna1.tab %_cdna2.tab %_comm.tab cdna.fasta
	!threads
	> $@; \
	for FILE in $< $^2 $^3; do \
	cut -f 1 < $$FILE >> $@; \
	done; \
	if [ -s $@ ]; then \
	get_fasta -m -f $@ < $^4 \
	| mafft.bat --thread $$THREADNUM --auto - \
	| sponge $@; \
	fi



%_query.faa: %_cdna1.tab %_cdna2.tab %_comm.tab %.fasta
	> $@; \
	for FILE in $< $^2 $^3; do \
	if [ -s $$FILE ]; then \
	cut -f 2 < $$FILE \
	| cut -f 1 -d ',' \
	| get_fasta -m -f <(cat -) < $^4 >> $@; \
	fi; \
	done


# uncollapsed
%_query_all.faa: %_cdna1.tab %_cdna2.tab %_comm.tab %.fasta
	> $@; \
	for FILE in $< $^2 $^3; do \
	if [ -s $$FILE ]; then \
	expandsets --separator ";" 2 < $$FILE \
	| cut -f 2 \
	| cut -f 1 -d ',' \
	| get_fasta -m -f <(cat -) < $^4 >> $@; \
	fi; \
	done



%_sbj.faa: %_cdna1.tab %_cdna2.tab %_comm.tab
	_comment () { echo -n ""; }; \
	> $@; \
	for FILE in $< $^2 $^3; do \
	if [ -s $$FILE ]; then \
	_comment "stick corresponding coverage values to contigs. when missing from the list, are singletons, so the coverage becomes 1"; \
	translate -a -v -e "1" -f 1 $(ASSEMBLY_1_COV) 1 < $$FILE \
	| bawk -v file=$$FILE '!/^[$$,\#+]/ { \
	# split for matching protein, i want take the first; \
	split($$7,a,";"); \
	# split for blast fields, i want take aligning part of the subject; \
	split(a[1],b,","); \
	# split for fields in query header, (first of blast fields), i want take query id (first of query header); \
	split(b[1],c," "); \
	# print contig, matching_protein, bit_score, belonging_set, per_base_average_coverage, piece_of_translated_contig_that_metch_by_blast \
	printf "%s %s %s %.3f %i-%i %.2f\t%s\n",$$1,c[1],file,b[3],b[6],b[7],$$3,b[5]; \
	}' \
	| bsort --key 1,1 \
	| tab2fasta 2 >> $@; \
	fi; \
	done


# uncollapsed
%_sbj_all.faa: %_cdna1.tab %_cdna2.tab %_comm.tab
	> $@; \
	for FILE in $< $^2 $^3; do \
	if [ -s $$FILE ]; then \
	expandsets --separator ";" 2 < $$FILE \
	| translate -a -v -e "1" -f 1 $(ASSEMBLY_1_COV) 1 \
	| bawk -v file=$$FILE '!/^[$$,\#+]/ { \
	# split for matching protein, i want take the first; \
	split($$7,a,";"); \
	# split for blast fields, i want take aligning part of the subject; \
	split(a[1],b,","); \
	# split for fields in query header, (first of blast fields), i want take query id (first of query header); \
	split(b[1],c," "); \
	# print contig, matching_protein, bit_score, belonging_set, per_base_average_coverage, piece_of_translated_contig_that_metch_by_blast \
	printf "%s %s %s %.3f %i-%i %.2f\t%s\n",$$1,c[1],file,b[3],b[6],b[7],$$3,b[5]; \
	}' \
	| bsort --key 1,1 \
	| tab2fasta 2 >> $@; \
	fi; \
	done



QUERY-SBJ_ALN = $(addsuffix _query-sbj.aln, $(GENES))
%_query-sbj.aln: %_query.faa %_sbj.faa
	!threads
	cat $< $^2 \
	| mafft.bat --thread $$THREADNUM --auto - > $@


QUERY-SBJ_ALN_ALL = $(addsuffix _query-sbj_all.aln, $(GENES))
%_query-sbj_all.aln: %_query_all.faa %_sbj_all.faa
	!threads
	cat $< $^2 \
	| mafft.bat --thread $$THREADNUM --auto - > $@



.PHONY: test
test:
	@export > export.all


# Standard Phony Targets for Users.

# This should be the default target.

# If I have a list of target, I report the list here. 
# For each element of the list, 
# make executes the rule that allows to build it.

# IMPORTANT: You must report all the target files for every
# rule
ALL   += $(SEQUENCES) \
	 $(LENGTHS) \
	 $(BLAST_RESULTS) \
	 cdna.fasta \
	 cdna.flag \
	 $(BLAST_FILTERED) \
	 $(BLAST_CDNA1) \
	 $(BLAST_CDNA2) \
	 $(BLAST_COMM) \
	 summary_$(ENSEMBL_VER)_$(FILTER_EVAL)_$(MIN_POS).tab \
	 $(SBJ_ALN) \
	 $(QUERY-SBJ_ALN) \
	 $(QUERY-SBJ_ALN_ALL)


# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE +=




# Delete all files in the current directory 
# that are normally created by building the program.
CLEAN += $(LENGTHS) \
	 cdna1_uniq.lst \
	 cdna2_uniq.lst \
	 cdna_comm.lst \
	 $(BLAST_FILTERED) \
	 $(BLAST_CDNA1) \
	 $(BLAST_CDNA2) \
	 $(BLAST_COMM) \
	 $(SBJ_ALN) \
	 $(QUERY-SBJ_ALN) \
	 $(QUERY-SBJ_ALN_ALL)





######################################################################
### phase_1.mk ends here
